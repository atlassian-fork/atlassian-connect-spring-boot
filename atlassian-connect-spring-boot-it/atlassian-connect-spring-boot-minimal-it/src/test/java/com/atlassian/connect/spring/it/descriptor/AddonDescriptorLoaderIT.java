package com.atlassian.connect.spring.it.descriptor;

import com.atlassian.connect.spring.internal.descriptor.AddonDescriptor;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.startsWith;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AddonDescriptorLoaderIT extends BaseApplicationIT {

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Test
    public void shouldReturnRawDescriptor() {
        String rawDescriptor = addonDescriptorLoader.getRawDescriptor();
        assertThat(rawDescriptor, startsWith("{"));
    }

    @Test
    public void shouldReturnAddonKey() {
        AddonDescriptor descriptor = addonDescriptorLoader.getDescriptor();
        assertThat(descriptor.getKey(), notNullValue());
    }

    @Test
    public void shouldReturnBaseUrl() {
        assertThat(addonDescriptorLoader.getDescriptor().getBaseUrl(), notNullValue());
    }

    @Test
    public void shouldReturnAuthenticationType() {
        assertThat(addonDescriptorLoader.getDescriptor().getAuthenticationType(), notNullValue());
    }

    @Test
    public void shouldReturnLifecycleUrls() {
        assertThat(addonDescriptorLoader.getDescriptor().getInstalledLifecycleUrl(), notNullValue());
        assertThat(addonDescriptorLoader.getDescriptor().getUninstalledLifecycleUrl(), notNullValue());
    }
}
