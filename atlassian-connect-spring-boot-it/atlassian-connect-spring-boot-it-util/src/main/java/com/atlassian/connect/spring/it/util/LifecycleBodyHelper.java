package com.atlassian.connect.spring.it.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class LifecycleBodyHelper {

    public static String createLifecycleJson(String eventType, String secret) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(LifecycleBodyHelper.createLifecycleEventMap(eventType, secret));
    }

    public static Map<String, Object> createLifecycleEventMap(String eventType) {
        return createLifecycleEventMap(eventType, AtlassianHosts.SHARED_SECRET);
    }

    public static Map<String, Object> createLifecycleEventMap(String eventType, String secret) {
        Map<String, Object> eventMap = new HashMap<>();
        eventMap.put("key", "atlassian-connect-spring-boot-test");
        eventMap.put("clientKey", AtlassianHosts.CLIENT_KEY);
        eventMap.put("publicKey", AtlassianHosts.PUBLIC_KEY);
        eventMap.put("serverVersion", "server-version");
        eventMap.put("pluginsVersion", "version-of-connect");
        eventMap.put("baseUrl", AtlassianHosts.BASE_URL);
        eventMap.put("displayUrl", AtlassianHosts.BASE_URL);
        eventMap.put("productType", AtlassianHosts.PRODUCT_TYPE);
        eventMap.put("description", "Atlassian JIRA at https://example.atlassian.net");
        eventMap.put("serviceEntitlementNumber", "SEN-number");
        eventMap.put("eventType", eventType);
        if (eventType.equals("installed")) {
            eventMap.put("sharedSecret", secret);
        }

        // add a field which is not part of the lifecycle event to ensure these do not trip up reception
        eventMap.put("installSong", "NeverGonnaGiveYouUp");

        return Collections.unmodifiableMap(eventMap);
    }
}
