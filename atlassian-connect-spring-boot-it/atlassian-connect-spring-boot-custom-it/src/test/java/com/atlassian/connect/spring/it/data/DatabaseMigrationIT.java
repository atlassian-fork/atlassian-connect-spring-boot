package com.atlassian.connect.spring.it.data;

import com.atlassian.connect.spring.it.TestRepository;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DatabaseMigrationIT extends BaseApplicationIT {

    @Autowired
    private TestRepository testRepository;

    @Test
    public void canUseRepositoryAfterLiquibaseMigration() {
        assertThat(testRepository.count(), is(0L));
    }
}
