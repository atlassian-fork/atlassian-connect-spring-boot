package com.atlassian.connect.spring.it.descriptor;

import com.atlassian.connect.spring.internal.descriptor.AddonDescriptor;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AddonDescriptorLoaderPropertyResolutionIT extends BaseApplicationIT {

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Test
    public void shouldResolveAddonKeyPlaceholder() {
        AddonDescriptor descriptor = addonDescriptorLoader.getDescriptor();
        assertThat(descriptor.getKey(), is("atlassian-connect-spring-boot-custom-it"));
    }

    @Test
    public void shouldResolveLifecycleUrlPlaceholders() {
        assertThat(addonDescriptorLoader.getDescriptor().getInstalledLifecycleUrl(), is("/custom-installed"));
        assertThat(addonDescriptorLoader.getDescriptor().getEnabledLifecycleUrl(), nullValue());
        assertThat(addonDescriptorLoader.getDescriptor().getDisabledLifecycleUrl(), nullValue());
        assertThat(addonDescriptorLoader.getDescriptor().getUninstalledLifecycleUrl(), is("/custom-uninstalled"));
    }
}
