package com.atlassian.connect.spring.internal.request;

import com.atlassian.connect.spring.AtlassianHost;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

public abstract class AtlassianConnectHttpRequestInterceptor implements ClientHttpRequestInterceptor {

    private static final String USER_AGENT_PRODUCT = "atlassian-connect-spring-boot";

    private String atlassianConnectClientVersion;

    public AtlassianConnectHttpRequestInterceptor(String atlassianConnectClientVersion) {
        this.atlassianConnectClientVersion = atlassianConnectClientVersion;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        HttpRequest interceptedRequest = getHostForRequest(request).map((host) -> wrapRequest(request, host)).orElse(request);
        return execution.execute(interceptedRequest, body);
    }

    protected abstract Optional<AtlassianHost> getHostForRequest(HttpRequest request);

    protected void assertRequestToHost(HttpRequest request, AtlassianHost host) {
        final URI requestUri = request.getURI();
        if (requestUri.isAbsolute() && !AtlassianHostUriResolver.isRequestToHost(requestUri, host)) {
            throw new IllegalArgumentException("Aborting the request to "
                    + requestUri.toASCIIString() +
                    " because this RestTemplate is associated with " + host.getBaseUrl());
        }
    }

    protected HttpRequest rewrapRequest(HttpRequest request, AtlassianHost host) {
        return request;
    }

    private HttpRequest wrapRequest(HttpRequest request, AtlassianHost host) {
        URI uri = wrapUri(request, host);
        HttpRequestWrapper requestWrapper = new AtlassianConnectHttpRequestWrapper(request, uri);
        return rewrapRequest(requestWrapper, host);
    }

    private URI wrapUri(HttpRequest request, AtlassianHost host) {
        URI uri = request.getURI();
        if (!uri.isAbsolute()) {
            URI baseUri = URI.create(host.getBaseUrl());
            uri = baseUri.resolve(getUriToResolve(uri, baseUri));
        }
        return uri;
    }

    private URI getUriToResolve(URI uri, URI baseUri) {
        String pathToResolve = "";
        String baseUriPath = baseUri.getPath();
        if (baseUriPath != null) {
            pathToResolve += baseUriPath;
        }
        String path = uri.getPath();
        if (path != null) {
            String pathToAppend = (pathToResolve.endsWith("/") && path.startsWith("/")) ? path.substring(1) : path;
            pathToResolve += pathToAppend;
        }

        try {
            uri = new URI(null, null, pathToResolve, uri.getQuery(), uri.getFragment());
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
        }
        return uri;
    }

    private class AtlassianConnectHttpRequestWrapper extends HttpRequestWrapper {

        private final URI uri;

        public AtlassianConnectHttpRequestWrapper(HttpRequest request, URI uri) {
            super(request);
            this.uri = uri;

            super.getHeaders().set(HttpHeaders.USER_AGENT,
                    String.format("%s/%s", USER_AGENT_PRODUCT, atlassianConnectClientVersion));
        }

        @Override
        public URI getURI() {
            return uri;
        }
    }
}
