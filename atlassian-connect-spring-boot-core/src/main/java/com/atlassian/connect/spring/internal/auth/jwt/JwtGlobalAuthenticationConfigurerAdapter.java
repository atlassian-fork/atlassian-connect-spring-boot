package com.atlassian.connect.spring.internal.auth.jwt;

import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.SecurityConfigurer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * A configuration class responsible for registering a {@link JwtAuthenticationProvider} without interfering with
 * auto-configured Basic authentication support.
 */
@Configuration
@Order(Ordered.LOWEST_PRECEDENCE)
public class JwtGlobalAuthenticationConfigurerAdapter extends GlobalAuthenticationConfigurerAdapter {

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Autowired
    private AtlassianHostRepository hostRepository;

    @Autowired
    private SecurityProperties securityProperties;

    @Autowired
    private ObjectProvider<PasswordEncoder> passwordEncoder;

    @Override
    public void init(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        JwtAuthenticationProvider jwtAuthenticationProvider = new JwtAuthenticationProvider(addonDescriptorLoader, hostRepository);
        authenticationManagerBuilder.apply(new JwtSecurityConfigurer(jwtAuthenticationProvider));

        UserDetailsServiceAutoConfiguration userDetailsServiceAutoConfiguration = new UserDetailsServiceAutoConfiguration();
        authenticationManagerBuilder.userDetailsService(userDetailsServiceAutoConfiguration.inMemoryUserDetailsManager(securityProperties, passwordEncoder));
    }

    private static class JwtSecurityConfigurer implements SecurityConfigurer<AuthenticationManager, AuthenticationManagerBuilder> {

        private final JwtAuthenticationProvider jwtAuthenticationProvider;

        public JwtSecurityConfigurer(JwtAuthenticationProvider jwtAuthenticationProvider) {
            this.jwtAuthenticationProvider = jwtAuthenticationProvider;
        }

        @Override
        public void init(AuthenticationManagerBuilder builder) throws Exception {}

        @Override
        public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
            authenticationManagerBuilder.authenticationProvider(jwtAuthenticationProvider);
        }
    }

}
