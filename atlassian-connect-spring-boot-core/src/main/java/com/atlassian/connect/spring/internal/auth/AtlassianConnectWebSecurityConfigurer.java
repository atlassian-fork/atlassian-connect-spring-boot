package com.atlassian.connect.spring.internal.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter.ReferrerPolicy;

@Configuration
@Order(SecurityProperties.BASIC_AUTH_ORDER + 1)
public class AtlassianConnectWebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    @Autowired
    private WebEndpointProperties webEndpointProperties;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers()
                .frameOptions().disable()
                .referrerPolicy().policy(ReferrerPolicy.ORIGIN_WHEN_CROSS_ORIGIN);
        http.csrf().disable();

        String managementPath = webEndpointProperties.getBasePath();
        if (managementPath != null) {
            http.authorizeRequests().antMatchers(managementPath + "/**").authenticated().and()
                    .httpBasic();
        }
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
