package com.atlassian.connect.spring.internal.request;

import com.atlassian.connect.spring.AtlassianHost;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class AtlassianHostUriResolverTest {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"http://example.com/", "http://example.com/", true},
                {"http://example.com/", "http://example.com/api", true},
                {"http://example.com/", "http://example.com/?some=value", true},
                {"http://example.com/", "https://example.com/", false},
                {"http://example.com/", "/api", true},
                {"http://other-host.com/", "https://example.com/", false}
        });
    }

    private final String baseUrl;
    private final String requestUrl;
    private final boolean expected;

    public AtlassianHostUriResolverTest(String baseUrl, String requestUrl, boolean expected) {
        this.baseUrl = baseUrl;
        this.requestUrl = requestUrl;
        this.expected = expected;
    }

    @Test
    public void shouldAcceptBaseUrlAsRequestToAuthenticatedHost() {
        assertThat(AtlassianHostUriResolver.isRequestToHost(
                URI.create(requestUrl), createHost(baseUrl)), is(expected));
    }

    private AtlassianHost createHost(String baseUrl) {
        AtlassianHost host = new AtlassianHost();
        host.setBaseUrl(baseUrl);
        return host;
    }
}
