<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>central-pom</artifactId>
        <version>5.0.15</version>
    </parent>

    <groupId>com.atlassian.connect</groupId>
    <artifactId>atlassian-connect-spring-boot</artifactId>
    <version>2.0.5-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>Atlassian Connect Spring Boot</name>
    <description>Provides support for Atlassian Connect add-ons using Spring Boot</description>
    <url>https://bitbucket.org/atlassian/atlassian-connect-spring-boot</url>

    <developers>
        <developer>
            <organization>Atlassian</organization>
        </developer>
    </developers>

    <modules>
        <module>atlassian-connect-spring-boot-api</module>
        <module>atlassian-connect-spring-boot-core</module>
        <module>atlassian-connect-spring-boot-jwt</module>
        <module>atlassian-connect-spring-boot-starter</module>
        <module>atlassian-connect-spring-boot-jpa-starter</module>
        <module>atlassian-connect-spring-boot-archetype</module>
    </modules>

    <scm>
        <url>https://bitbucket.org/atlassian/atlassian-connect-spring-boot</url>
        <connection>scm:git:ssh://git@bitbucket.org/atlassian/atlassian-connect-spring-boot.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/atlassian/atlassian-connect-spring-boot.git</developerConnection>
      <tag>HEAD</tag>
    </scm>
    <issueManagement>
        <system>JIRA</system>
        <url>https://ecosystem.atlassian.net/browse/ACSPRING</url>
    </issueManagement>

    <properties>
        <!-- Spring versions -->
        <spring-boot.version>2.2.5.RELEASE</spring-boot.version>
        <spring-security-oauth2.version>2.4.0.RELEASE</spring-security-oauth2.version>

        <!-- Other dependency versions -->
        <nimbus-jose-jwt.version>7.9</nimbus-jose-jwt.version>

        <!-- Build and test versions -->
        <jacoco.version>0.8.4</jacoco.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>org.springframework.security.oauth</groupId>
                <artifactId>spring-security-oauth2</artifactId>
                <version>${spring-security-oauth2.version}</version>
            </dependency>
            <dependency>
                <groupId>com.nimbusds</groupId>
                <artifactId>nimbus-jose-jwt</artifactId>
                <version>${nimbus-jose-jwt.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <configuration>
                        <compilerArgs>
                            <arg>-Xlint:unchecked</arg>
                            <arg>-Werror</arg>
                        </compilerArgs>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>${jacoco.version}</version>
                    <executions>
                        <execution>
                            <id>prepare-agent</id>
                            <goals>
                                <goal>prepare-agent</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>tidy-maven-plugin</artifactId>
                <version>1.0.0</version>
                <executions>
                    <execution>
                        <id>check</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <executions>
                    <execution>
                        <id>dependency-convergence</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <dependencyConvergence/>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>checkstyle</id>
            <properties>
                <atlassian.codestyle>platform:1.0.1</atlassian.codestyle>
            </properties>
            <repositories>
                <repository>
                    <id>atlassian-public</id>
                    <url>https://maven.atlassian.com/repository/public</url>
                </repository>
            </repositories>
        </profile>
        <profile>
            <id>it</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <modules>
                <module>atlassian-connect-spring-boot-it</module>
            </modules>
        </profile>
    </profiles>
</project>
